const REFRESH_TOKEN_LIFESPAN = "1d";

const ACCESS_TOKEN_LIFESPAN = "40m";
const AUTH_COOKIE_LIFESPAN = 6 * 60 * 60 * 1000;

export { ACCESS_TOKEN_LIFESPAN, AUTH_COOKIE_LIFESPAN, REFRESH_TOKEN_LIFESPAN };
