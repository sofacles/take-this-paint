import express from "express";
import fs from "fs/promises";
import { PaintCanModel } from "../../data/models";
import { deleteFile, getS3FileUrl } from "../../data/s3";
import verifyToken from "../../checkLoginMiddleware";

const router = express.Router();

const getPaints = async (_, res, next) => {
  try {
    const paints = await PaintCanModel.find({});
    const adjustedPaints = Array<typeof PaintCanModel>();
    for (let paint of paints) {
      if (paint.imageName) {
        //need to prepend the url to the image name
        paint.imageName = getS3FileUrl(paint.imageName);
      }
    }
    res.status(200).json(paints);
    next();
  } catch (error) {
    res.status(500).json({ err: error });
  }
};

const deletePaint = async (req, res) => {
  let doomedPaint = await PaintCanModel.findOne({ _id: req.query.id });
  if (doomedPaint.imageName) {
    try {
      await deleteFile(doomedPaint.imageName);
    } catch (error) {
      console.log(error);
      return res.send({
        status: 200,
        data: {
          result: "delete image failed",
        },
      });
    }
  }

  let deleteResult = await PaintCanModel.deleteOne({ _id: req.query.id });
  res.send({
    status: 200,
    data: {
      result: deleteResult.deletedCount === 1 ? "success" : "deleteFailed",
    },
  });
};

router.get("/", verifyToken, getPaints);
router.delete("/", verifyToken, deletePaint);

export default router;
